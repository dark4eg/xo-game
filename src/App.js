import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './App.css';
import { actions as gameActions } from './redux/modules/game';
import Map from './components/Map';
import Table from './components/Table';
import StrikeSelect from "./components/StrikeSelect";

class App extends Component {
    propTypes = {
        startGame: PropTypes.func.isRequired,
        stepGame: PropTypes.func.isRequired,
        resumeGame: PropTypes.func.isRequired,
        changeStrike: PropTypes.func.isRequired,
        game: PropTypes.shape({
            settings: PropTypes.shape({
                winCountCurrent: PropTypes.number.isRequired,
                winCountList: PropTypes.arrayOf(PropTypes.number).isRequired,
            }),
            steps: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number.isRequired,
            })),
            next: PropTypes.oneOf ([undefined, "X", "0"]),
            win: PropTypes.oneOf ([undefined, "X", "0"]),
        }),
        winsX: PropTypes.number.isRequired,
        wins0: PropTypes.number.isRequired,
    };

    render() {
        const { startGame, stepGame, resumeGame, game, winsX, wins0, changeStrike } = this.props;

        console.log('game', game);

        return (
            <div className="container">
                <div className="app">
                    <StrikeSelect settings={game.settings} changeStrike={changeStrike}/>
                    <Map
                        game={game.game}
                        stepGame={stepGame}
                        status={game.status}/>
                    <Table
                        startGame={startGame}
                        status={game.status}
                        game={game.game}
                        winsX={winsX}
                        wins0={wins0}
                        resumeGame={resumeGame}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    game: state.game,
    winsX: state.game.users["X"].wins,
    wins0: state.game.users["0"].wins,
});
const mapActionsToProps = {
    startGame: gameActions.startGame,
    stepGame: gameActions.stepGame,
    resumeGame: gameActions.resumeGame,
    changeStrike: gameActions.changeStrike,
};

export default connect(mapStateToProps, mapActionsToProps)(App);
